package game

import (
  "gitlab.com/nathanwillson/hebi/web/api"
)

func NextMove(s *api.SnakeRequest) string {
	var move string

  w := BuildWorld(s)

	move = SurvivalMove(w)

  return move
}
