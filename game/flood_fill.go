package game

import (
  "fmt"
)

func FloodFill(xStart int, yStart int, w *World) *Grid {
  g := w.Grid

  // Dupe grid points
  points := make([]Point, len(*g.Points))
  copy(points, *g.Points)

  g_dupe := &Grid{g.Height,g.Width, &points}

  // Default non-zero value
  g_dupe.SetValue(xStart,yStart, float32(-1))

  fill(emptySurrPoints(xStart, yStart,g_dupe), g_dupe, 1)

  return g_dupe
}


func fill(openPoints []Coord, g *Grid, steps int) {
  if len(openPoints) == 0 {
    return
  }

  var nextPoints []Coord
  nextPointsLedger := make(map[string]bool)

  for _, o := range openPoints {
    fps := floodPoint(o, g, steps)

    for _, fp := range fps {
      key := fmt.Sprintf("%d%d", fp.X, fp.Y)

      if(nextPointsLedger[key]) {
        continue
      } else {
        nextPointsLedger[key] = true
        nextPoints = append(nextPoints, fp)
      }

    }
  }

  fill(nextPoints, g, steps + 1)
}

func floodPoint(c Coord, g *Grid, v int) []Coord {
  var nextCoords []Coord

  g.SetValue(c.X,c.Y, float32(v))

  if g.Get(c.X, c.Y).IsEmpty() {
    nextCoords = emptySurrPoints(c.X, c.Y, g)
  }

  return nextCoords
}

// // Empty surrounding points that are untouched
func emptySurrPoints(x int, y int, g *Grid) []Coord {
	var c []Coord

  for i := -1; i <= 1; i++ {
    for j := -1; j <= 1; j++ {
      // Skip if not left, up, right, down
      if intAbs(i+j) != 1 {
        continue
      }

      // if within bounds
      // if empty
      if g.IsWithinBounds(x+i, y+j) {
        if p := g.Get(x+i, y+j); p.Value == 0 {
          c = append(c, Coord{x+i,y+j})
        }
      }

    }
  }

  return c
}

func intAbs(x int) int {
  if x < 0 {
    return -x
  }
  return x
}
