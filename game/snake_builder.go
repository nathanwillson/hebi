package game

import (
  "gitlab.com/nathanwillson/hebi/web/api"
)

func BuildSnakes(s *api.SnakeRequest) *[]Snake {
  sneks := s.Board.Snakes
  snakes := make([]Snake, len(sneks))

  for  i := 0; i < len(snakes); i++ {
    snakes[i] = BuildSnake(sneks[i])
  }

  return &snakes
}

func BuildSnake(s api.Snake) Snake {
  body := make([]Coord, len(s.Body))

  for  i := 0; i < len(s.Body); i++ {
    body[i] = Coord{
      X: s.Body[i].X,
      Y: s.Body[i].Y,
    }
  }

  return Snake{
    Id: s.ID,
    Name: s.Name,
    Health: s.Health,
    Body: body,
  }
}

