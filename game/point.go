package game

const MY_HEAD = "q"
const MY_BODY = "q_h"
const ENEMY_BODY = "e"
const ENEMY_HEAD = "e_h"
const FOOD = "f"

type Point struct {
  Type string
  Value float32
}

func (p Point) SetSnake() Point {
	p.Type = ENEMY_BODY
  return p
}

func (p Point) SetSnakeHead() Point {
	p.Type = ENEMY_HEAD
  return p
}

func (p Point) SetMySnake() Point {
	p.Type = MY_BODY
  return p
}

func (p Point) SetMySnakeHead() Point {
	p.Type = MY_HEAD
  return p
}

func (p Point) SetFood() Point {
	p.Type = FOOD
  return p
}

func (p Point) SetValue(v float32) Point {
	p.Value = v
  return p
}

func (p Point) IsEmpty() bool {
	return p.Type == ""
}

func (p Point) IsFood() bool {
	return p.Type == FOOD
}

func (p Point) IsSnakeHead() bool {
	return (p.Type == ENEMY_HEAD) || (p.Type == MY_HEAD)
}

func (p Point) IsSnakeBody() bool {
	return (p.Type == ENEMY_BODY) || (p.Type == MY_BODY)
}

func (p Point) IsSnake() bool {
	return p.IsSnakeBody() || p.IsSnakeHead()
}

func (p Point) IsMySnakeHead() bool {
	return p.Type == MY_HEAD
}

func (p Point) IsMySnake() bool {
	return (p.Type == MY_BODY) || (p.Type == MY_HEAD)
}
