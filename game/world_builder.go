package game

import (
  "gitlab.com/nathanwillson/hebi/web/api"
_	"github.com/k0kubun/pp"
)

func BuildWorld(s *api.SnakeRequest) *World {
	grid := BuildGrid(s)
	snakes := BuildSnakes(s)
	food := BuildFood(s)

	w := World{
		Food: food,
		Grid: grid,
		Snakes: snakes,
		MyId: s.You.ID,
	}

	return &w
}

