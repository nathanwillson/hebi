package game

import (
  "gitlab.com/nathanwillson/hebi/web/api"
)

func BuildFood(s *api.SnakeRequest) *[]Coord {
  food := s.Board.Food
  var coords []Coord

  for _, f := range food {
    coords = append(coords, Coord{
      X: f.X,
      Y: f.Y,
    })
  }

  return &coords
}
