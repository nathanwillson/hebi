package game

import (
	"fmt"

	. "github.com/logrusorgru/aurora"
)

type Grid struct {
  Height int
  Width int
  Points *[]Point
}

func (g *Grid) Get(x int, y int) Point {
	return (*g.Points)[x + y*g.Width]
}

func (g *Grid) IsWithinBounds(x int, y int) bool {
  return (x >= 0) && (x < g.Width) && (y >= 0) && (y < g.Height)
}

func (g *Grid) SetSnake(x int, y int) {
  (*g.Points)[x + y*g.Width] = g.Get(x,y).SetSnake()
}

func (g *Grid) SetSnakeHead(x int, y int) {
  (*g.Points)[x + y*g.Width] = g.Get(x,y).SetSnakeHead()
}

func (g *Grid) SetMySnake(x int, y int) {
  (*g.Points)[x + y*g.Width] = g.Get(x,y).SetMySnake()
}

func (g *Grid) SetMySnakeHead(x int, y int) {
  (*g.Points)[x + y*g.Width] = g.Get(x,y).SetMySnakeHead()
}

func (g *Grid) SetFood(x int, y int) {
  (*g.Points)[x + y*g.Width] = g.Get(x,y).SetFood()
}

func (g *Grid) SetValue(x int, y int, v float32) {
  (*g.Points)[x + y*g.Width] = g.Get(x,y).SetValue(v)
}

func (g *Grid) Print() {
  points := *g.Points

  for i := 0 ; i < len(points); i++ {
    p := points[i]

    if p.IsEmpty() {
      fmt.Printf(" %s ", White("■"))
    } else if p.IsFood() {
      fmt.Printf(" %s ", Red("■"))
    } else if p.IsMySnakeHead() {
      fmt.Printf(" %s ", Magenta("■").Bold())
    } else if p.IsMySnake() {
      fmt.Printf(" %s ", Magenta("■"))
    } else if p.IsSnakeHead() {
      fmt.Printf(" %s ", Green("■").Bold())
    } else {
      fmt.Printf(" %s ", Green("■"))
    }

    if (i+1)%g.Height == 0 {
      fmt.Println("")
    }
  }
}

func (g *Grid) PrintValues() {
  points := *g.Points

  fmt.Println("----------")
  for i := 0 ; i < len(points); i++ {
    p := points[i]

    s := fmt.Sprintf("%2.f", p.Value)

    if p.IsEmpty() {
      fmt.Printf(" %s ", White(s))
    } else if p.IsFood() {
      fmt.Printf(" %s ", Red(s))
    } else if p.IsMySnakeHead() {
      fmt.Printf(" %s ", Magenta(s).Bold())
    } else if p.IsMySnake() {
      fmt.Printf(" %s ", Magenta(s))
    } else if p.IsSnakeHead() {
      fmt.Printf(" %s ", Green(s).Bold())
    } else {
      fmt.Printf(" %s ", Green(s))
    }

    if (i+1)%g.Height == 0 {
      fmt.Println("")
    }
  }
}
