package game

import (
	"testing"
)

func TestReduceMap(t *testing.T) {
  test := map[int]int{
    10: 9,
    9: 8,
    6: 4,
    5: 3,
    3: 2,
    2: 1,
  }
  result := ReduceMap(test)

  correct := map[int]int{
    10: 8,
    9: 8,
    6: 4,
    5: 1,
    3: 1,
    2: 1,
  }

  for k := range correct {
    if result[k] != correct[k] {
      t.Error(
        "For", result,
        "expected", correct,
      )
    }
  }
}

