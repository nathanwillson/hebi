package game

import (
 "errors"
)

type Snake struct {
  Id string
  Name string
  Health int
  Body []Coord
}

func (s Snake) Head() (Coord, error) {
	if len(s.Body) == 0 {
		return Coord{}, errors.New("Empty snake. No head.")
	}

	return s.Body[0], nil
}
