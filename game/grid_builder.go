package game

import (
  "gitlab.com/nathanwillson/hebi/web/api"
)

func BuildGrid(s *api.SnakeRequest) *Grid {
  max := s.Board.Height * s.Board.Width
  points := make([]Point, max)

  grid := Grid {
    Height: s.Board.Height,
    Width: s.Board.Width,
    Points: &points,
  }

  addSnakes(s, &grid)
  addFood(s, &grid)

  return &grid
}

func addSnakes(s *api.SnakeRequest, g *Grid) {
  snakes := s.Board.Snakes

  for i := 0; i < len(snakes); i++ {
    body := snakes[i].Body
    for b := 0; b < len(body); b++ {
      g.SetSnake(body[b].X, body[b].Y)
    }

    if len(body) > 0 {
      g.SetSnakeHead(body[0].X, body[0].Y)
    }
  }

  me := s.You.Body

  for b := 0; b < len(me); b++ {
    g.SetMySnake(me[b].X, me[b].Y)
  }
  if len(me) > 0 {
    g.SetMySnakeHead(me[0].X, me[0].Y)
  }
}

func addFood(s *api.SnakeRequest, g *Grid) {
  food := s.Board.Food

  for i := 0; i < len(food); i++ {
    g.SetFood(food[i].X, food[i].Y)
  }
}
