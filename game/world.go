package game

import (
 "errors"
)

type World struct {
	Food *[]Coord
	Grid *Grid
	Snakes *[]Snake
	MyId string
}


func (w *World) MySnake() (Snake, error) {
	snakes := *w.Snakes

	for i := 0; i < len(snakes); i++ {
		if snakes[i].Id == w.MyId {
			return snakes[i], nil
		}
	}

	return Snake{}, errors.New("Cannot find my snake!")
}

