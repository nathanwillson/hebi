package game

import (
	"sort"
  _	"github.com/k0kubun/pp"
)

const DefaultDir = "left"

// Used to store UP, DOWN, LEFT, RIGHT
type Direction struct {
	Dir string
	Delta []int
}

// Stores direction string and resulting Coord
type Destination struct {
	Dir string
	Dest Coord
}

func SurvivalMove(w *World) string {
	s, err := w.MySnake()

	if err != nil {
		return DefaultDir
	}

	h, err := s.Head()

	if err != nil {
		return DefaultDir
	}

  // =================================
  // "Needs food" Analysis
  //
  // Find if the snake should eat:
  //
  // 1. Flood fill
  // 2. Find the closest food
  // 3. Compare snake Health to closest food distance
  //
  if len(*w.Food) > 0 {
    floodGrid := FloodFill(h.X, h.Y, w)

    // Find the closest food
    var food Coord
    var foodDist int
    for _, f := range *w.Food {
      // Get Int value of Food and check if non-zero
      if v := int(floodGrid.Get(f.X, f.Y).Value); v != 0 {
        // Save the closest food
        if (foodDist > v) || (foodDist == 0) {
          foodDist = v
          food = f
        }
      }
    }

    threshold := 10
    if len(s.Body) > floodGrid.Width {
      threshold = floodGrid.Width
    }

    // If a food was found..
    // Go towards the food if needed
    if (foodDist != 0) && (s.Health - threshold < foodDist) {

      curr := food

      for i := foodDist; i > 0; i-- {
        for _, dir := range allDirections() {
          x := curr.X + dir.Delta[0]
          y := curr.Y + dir.Delta[1]

          if floodGrid.IsWithinBounds(x,y) {
            if p := floodGrid.Get(x,y); (int(p.Value) == i) && p.IsEmpty() {
              curr = Coord{x,y}
              break
            }
          }
        }
      }


      switch {
      case (curr.X - h.X) == 1:
        return "right"
      case (curr.X - h.X) == -1:
        return "left"
      case (curr.Y - h.Y) == 1:
        return "down"
      case (curr.Y - h.Y) == -1:
        return "up"
      }
    }

  }

  // =================================
  // Connected Component analysis

  // - Look at all connected compoents to head
  // - If more than one connected component, go towards
  //   the one with bigest area
  cc := ConnectedComponents(w)
  ccC := openCoords(h, cc.Grid)
	if len(ccC) > 1 {

    // First off, check if connected to more than
    // one component
    var multComp bool
    base := cc.Grid.Get(ccC[0].Dest.X, ccC[0].Dest.Y)
    for _, c := range ccC {
      if base != cc.Grid.Get(c.Dest.X, c.Dest.Y) {
        multComp = true
        break
      }
    }

    // If connected to multiple components
    if multComp {
      var label, maxArea int
      var comp Comp
      var d Destination

      for _, c := range ccC {
        label = int(cc.Grid.Get(c.Dest.X, c.Dest.Y).Value)
        comp = cc.Components[label]

        if comp.Area > maxArea {
          maxArea = comp.Area
          d = c
        }
      }

      return d.Dir
    }
  }


  // =================================
  // Least edges analysis

	// Find open Coords from HEAD
	d := openCoords(h, w.Grid)

	if len(d) == 0 {
		return DefaultDir
	}

	// For each open direction,
  // - sort coords by least number of open spots
  // - defer to dirToInt(..) if edge value is the same
	var o1, o2, d1, d2 int
	sort.SliceStable(d, func(i, j int) bool {
		o1 = len(openCoords(d[i].Dest, w.Grid))
		o2 = len(openCoords(d[j].Dest, w.Grid))

		if o1 < o2 {
			return true
		}
		if o1 > o2 {
			return false
		}

		// Defer fixed order when o1 == o2
		d1 = dirToInt(d[i].Dir)
		d2 = dirToInt(d[j].Dir)
		return (d1 < d2)
	})

  return d[0].Dir
}

// Returns open coordinates around a point (with the direction taken to that point)
func openCoords(startPoint Coord, g *Grid) []Destination{
	var openDirs []Destination

	d := allDirections();

	for i := 0; i < len(d); i++ {
		x := startPoint.X + d[i].Delta[0]
		y := startPoint.Y + d[i].Delta[1]

		if g.IsWithinBounds(x,y) {
			if p := g.Get(x,y); p.IsEmpty() || p.IsFood() {
				openDirs = append(openDirs, Destination{d[i].Dir, Coord{x,y}})
			}
		}
	}

	return openDirs
}

// Vector constants for LEFT, RIGHT, DOWN, UP
func left() []int { return []int{-1, 0} }
func right() []int { return []int{1, 0} }
func up() []int { return []int{0, -1} }
func down() []int { return []int{0, 1} }
func allDirections() []Direction {
	return []Direction{
		Direction{ "left", left()},
		Direction{ "right", right()},
		Direction{ "up", up()},
		Direction{ "down", down()},
	}
}
// For sorting
func dirToInt(dir string) int {
	switch dir {
	case "left":
		return 1
	case "up":
		return 2
	case "right":
		return 3
	case "down":
		return 4
	default:
		return 5
	}
}

