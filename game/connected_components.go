package game

// Implementing a two pass connected component analysis
// as described here:
// https://en.wikipedia.org/wiki/Connected-component_labeling#Two-pass

import (
)

type Comp struct {
  Val int
  Area int
}
type ConnComp struct {
  Grid *Grid
  Components map[int]Comp
}

func ConnectedComponents(w *World) ConnComp {
  g_orig := w.Grid

 // Dupe grid points
  points := make([]Point, len(*g_orig.Points))
  copy(points, *g_orig.Points)

  grid := &Grid{g_orig.Height, g_orig.Width, &points}

  // First pass
  g, ledger := firstPass(grid)

  // Second pass
  c := secondPass(grid, ledger)

  return ConnComp{g, c}
}

// Reduces the ledger so that each key points
// to the minimum component number
//
// Required for second pass
func ReduceMap(m map[int]int) map[int]int {
  for k := range m {
    v, ok := m[m[k]]

    for ; ok; {
      _, ok = m[v]

      if ok {
        v = m[v]
      } else {
        m[k] = v
      }
    }
  }

  return m
}

// Iterates over the grid and labels components (and creates a ledger of connected components)
//
// As described https://en.wikipedia.org/wiki/Connected-component_labeling#Two-pass
func firstPass(g *Grid) (*Grid, map[int]int) {
  var x, y  int
  label := 1 // Component labeling starts at 1
  ledger := make(map[int]int)

  for i := 0; i < g.Width*g.Height; i++ {
    x = i%g.Width
    y = i/g.Width

    if g.Get(x,y).IsSnake() {
      continue
    }

    neigh := getNeighbours(g, x, y)

    if len(neigh) == 0 {
      g.SetValue(x,y, float32(label))
      label += 1
    } else {
      // Find min label of neighbours
      minLabel := label
      labels   := make(map[int]int)
      for _, n := range neigh {
        v := int(g.Get(n.X, n.Y).Value)

        labels[v] = labels[v] + 1

        if v < minLabel{
          minLabel = v
        }
      }

      // Set value to min label
      g.SetValue(x,y, float32(minLabel))

      // Save in ledger which components are connected
      for l := range labels {
        if l == minLabel {
          continue
        }

        ledger[l] = minLabel
      }
    }
  }

  ledger = ReduceMap(ledger)

  return g, ledger
}

// Merges any connected components using the ledger for lookup
//
// Returns map of Comp's with area of each component
//
// As described https://en.wikipedia.org/wiki/Connected-component_labeling#Two-pass
func secondPass(g *Grid, ledger map[int]int) map[int]Comp {
  comps := make(map[int]Comp)

  var x, y, v, key int
  var ok bool
  var c Comp
  for i := 0; i < g.Width*g.Height; i++ {
    x = i%g.Width
    y = i/g.Width

    if g.Get(x,y).IsSnake() {
      continue
    }

    key = int(g.Get(x,y).Value)
    v, ok = ledger[key]
    if ok {
      g.SetValue(x,y, float32(v))
      key = v
    }

    c, ok = comps[key]
    if ok {
      c.Area++
      comps[key] = c
    } else {
      comps[key] = Comp{key, 1}
    }
  }

  return comps
}

// Get neighbouring LEFT or UP neighbours that are non-snake
func getNeighbours(g *Grid, x,y int) []Coord {
  var c []Coord

  // Left
  if g.IsWithinBounds(x-1, y) {
    if p :=g.Get(x-1,y); !p.IsSnake() {
      c = append(c, Coord{x-1, y})
    }
  }

  // Up
  if (g.IsWithinBounds(x, y-1)) {
    if p :=g.Get(x,y-1); !p.IsSnake() {
      c = append(c, Coord{x, y-1})
    }
  }

  return c
}


