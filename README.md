# Hebi

Battlesnake written in Go.

## Battlesnake

1. [Main site](https://play.battlesnake.com/)
2. [Docs](https://docs.battlesnake.com/)

# Step 1: Survival-snake (single snake)

Uses:
*  Flood fill
*  Connected component analysis

![survival snake](/static/img/bs.gif)

# Step 2: (working on it..)

## Installation

`go get -v gitlab.com/nathanwillson/hebi`

## Running the app

`go run cmd/hebi/main.go`

Or use the `Makefile`.


