run:
	hebi server
.PHONY: run

fmt:
	@echo ">> Running Gofmt.."
	gofmt -l -s -w .

get:
	@echo ">> Getting any missing dependencies.."
	go get ./...

install:
	go install gitlab.com/nathanwillson/hebi

tests:
	go test -v ./...




