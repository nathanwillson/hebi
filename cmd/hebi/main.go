package main

import (
	"log"
	"net/http"

  "gitlab.com/nathanwillson/hebi/web"
)

func main() {
    web.SetupRoutes()
    log.Printf("Running server on port %s...\n", "9999")
		log.Println(http.ListenAndServe(":9999", nil))
}
