package controller

import (
	"fmt"
	"net/http"

  "gitlab.com/nathanwillson/hebi/web/api"
	log "github.com/sirupsen/logrus"
)

func Start(res http.ResponseWriter, req *http.Request) {
	decoded := api.SnakeRequest{}
	err := api.DecodeSnakeRequest(req, &decoded)
	if err != nil {
		log.Error(fmt.Errorf("Bad start request: %v", err))
		dump(decoded)
	}

	respond(res, api.StartResponse{
		Color: "#75CEDD",
	})
}

