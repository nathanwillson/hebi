package controller

import (
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"

  "gitlab.com/nathanwillson/hebi/web/api"
  "gitlab.com/nathanwillson/hebi/game"
)

func Move(res http.ResponseWriter, req *http.Request) {
	decoded := api.SnakeRequest{}
	err := api.DecodeSnakeRequest(req, &decoded)
	if err != nil {
		log.Error(fmt.Errorf("Bad move request: %v", err))
		dump(decoded)
	}

	move := game.NextMove(&decoded)

	respond(res, api.MoveResponse{
		Move: move,
	})
}
