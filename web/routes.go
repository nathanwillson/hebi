package web

import (
	"net/http"
  "time"

  c "gitlab.com/nathanwillson/hebi/web/controller"
	log "github.com/sirupsen/logrus"
)

func SetupRoutes() {
	http.HandleFunc("/", Index)
	http.HandleFunc("/end", logHandler(c.End))
	http.HandleFunc("/move", logHandler(c.Move))
	http.HandleFunc("/ping", logHandler(c.Ping))
	http.HandleFunc("/start", logHandler(c.Start))
}

func logHandler(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		h(w, r)
		log.WithFields(log.Fields{
			"duration": time.Since(start),
			"url":      r.URL.String(),
		}).Info("")
	}
}



func Index(res http.ResponseWriter, req *http.Request) {
	res.WriteHeader(http.StatusOK)
	res.Write([]byte("🐍"))
}






